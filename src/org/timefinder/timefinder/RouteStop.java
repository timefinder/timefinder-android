package org.timefinder.timefinder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class RouteStop extends Activity {
	String routeNumber;
	String unescapedName;
	String escapedName;
	String stopCode;
	String stopName;
	List<String> stopTimes;
	List<String[]> allTimes;
	int prevDay = 0;
	int nextDay = 0;
	
	private class DownloadDataTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... v) {
			SharedPreferences settings = getSharedPreferences("timefinder_prefs", 0);
			String serverurl = settings.getString("serverurl", getString(R.string.serverurl));
			String urlstring = serverurl+"stops/"+stopCode+".csv";
			URL url;
			try {
				url = new URL(urlstring);
			} catch (MalformedURLException e2) {
				return "Internal Error";
			}
			
			InputStream inputStream;
			try {
				inputStream = url.openStream();
				InputStreamReader irs = new InputStreamReader(inputStream);
				BufferedReader br = new BufferedReader(irs);
				String line;
				allTimes = new ArrayList<String[]>();
				while ((line = br.readLine()) != null) {
					String[] row = line.split(",");
					if (row.length >= 9) {
						if (row[0].equals(routeNumber)) {
							allTimes.add(row);
						}
					}
				}
			} catch(FileNotFoundException e) {
			    return "API Error";
			} catch (IOException e1) {
				return "Network Error";
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (result == null || result == "") {
				RouteStop.this.getTimeTable(0);
				
				ViewSwitcher vs = (ViewSwitcher) findViewById(R.id.switcher);
				if (vs.getDisplayedChild() == 0) {
					vs.showNext();
				}
			}
			else {
				Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
			}
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stop);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		Bundle extras = getIntent().getExtras();
		routeNumber = extras.getString("route_number");
		unescapedName = extras.getString("unescaped_name");
		escapedName = extras.getString("escaped_name");
		stopCode = extras.getString("stop_code");
		stopName = extras.getString("stop_name");
		
		TextView t = (TextView) findViewById(R.id.route_number);
		t.setText(routeNumber);
		
		new DownloadDataTask().execute();
	}
	
	private int d(int day) {
		if (day == 1) day = 8;
		return day;
	}
	
	public void getTimeTable(int day) {
		Log.w("RouteStop", "getTimeTable("+Integer.toString(day)+")");
		Calendar c = Calendar.getInstance();
		if (day == 0) {
			day = d(c.get(Calendar.DAY_OF_WEEK));
		}
		else {
			while (d(c.get(Calendar.DAY_OF_WEEK)) != day) {
				c.add(Calendar.DATE, 1);
			}
		}
		
		TextView t1 = (TextView) findViewById(R.id.stop_name);
		t1.setText(stopName);
		SimpleDateFormat shortDayText = new SimpleDateFormat("EEE");
		TextView t2 = (TextView) findViewById(R.id.stop_day);
		t2.setText(shortDayText.format(c.getTime()));
		
		c.add(Calendar.DATE, -1);
		Button p = (Button) findViewById(R.id.stop_prev);
		prevDay = d(c.get(Calendar.DAY_OF_WEEK));
		p.setText("<- "+shortDayText.format(c.getTime()));
		p.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.w("RouteStop", "ClickTest");
				getTimeTable(prevDay);
			}
		});
		
		c.add(Calendar.DATE, 2);
		nextDay = d(c.get(Calendar.DAY_OF_WEEK));
		Button n = (Button) findViewById(R.id.stop_next);
		n.setText(shortDayText.format(c.getTime())+" ->");
		n.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				getTimeTable(nextDay);
			}
		});
		
		stopTimes = new ArrayList<String>();
		for (String[] row : allTimes) {
			if (row[day].equals("1")) {
				stopTimes.add(row[1]);
			}
		}
		
        GridView g = (GridView) findViewById(R.id.stoptimes);
        StopTimeAdapter stopTimeAdapter = new StopTimeAdapter(this, R.layout.time, stopTimes);
        g.setAdapter(stopTimeAdapter);
	}
	
	private class StopTimeAdapter extends ArrayAdapter<String> {
    	Context mContext;
		int mResource;
		List<Boolean> firstInHour;

		public StopTimeAdapter(Context context, int layout, List<String> l) {
    		super(context, layout, l);
    		mContext = context;
    		mResource = layout;
    		String lastHour = "-1";
    		firstInHour = new ArrayList<Boolean>();
    		for (String stop : l) {
    			String hour = stop.substring(0,2);
    			if (hour.equals(lastHour)) {
    				firstInHour.add(false);
    			}
    			else {
    				firstInHour.add(true);
    			}
    			lastHour = hour;
    		}
    	}
    	
    	@Override
        public View getView(int position, View convertView, ViewGroup parent) {
    		View view;
    		if (convertView == null) {
    			LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(mResource, parent, false);
            } else {
                view = convertView;
            }
    		String time = getItem(position);
    		if (firstInHour.get(position) && time.length() >= 4) {
        		((TextView)view.findViewById(R.id.time_text_bold)).setText(time.substring(0, 2));
        		((TextView)view.findViewById(R.id.time_text)).setText(time.substring(2,4));
    		}
    		else {
        		((TextView)view.findViewById(R.id.time_text_bold)).setText("");
    			((TextView)view.findViewById(R.id.time_text)).setText(time);
    		}
    		return view;
    	}
    };
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.stop, menu);
	    return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
        HashMap<String, Void> favStops = TimeFinder.favStops;
	    MenuItem i = menu.findItem(R.id.menu_add_fav);
        String id = stopCode;
	    if (favStops.containsKey(id)) {
	    	i.setTitle(R.string.un_fav);
	    }
	    else {
	    	i.setTitle(R.string.add_fav);
	    }
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
	    switch (item.getItemId()) {
		    case R.id.menu_add_fav:
		        HashMap<String, Void> favStops = TimeFinder.favStops;
		        String id = stopCode;
		    	if (favStops.containsKey(id)) {
		    		favStops.remove(id);
		    	}
		    	else {
			    	favStops.put(id, null);
		    	}
		        /*String starchar = "";
			    if (starredPlings.containsKey(id)) {
				    starchar = " ♥";
			    }*/
		        //((TextView)findViewById(R.id.activity_name))
		        // 		.setText(activity.get("Name")+starchar);
		    	return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
	}
}
