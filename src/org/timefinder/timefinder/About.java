package org.timefinder.timefinder;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class About extends Activity implements OnClickListener {
	TextView searchText;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flip);
        
        //searchText = (TextView) findViewById(R.id.routesearch_text);
        //Button b = (Button) findViewById(R.id.routesearch_button);
        //b.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		/**/
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.about, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    Intent intent;
	    switch (item.getItemId()) {
		    case R.id.menu_donate:
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=BGH5NHRHJ7XQ6"));
		        startActivity(intent);
		        return true;
		    case R.id.menu_twitter_josh:
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://twitter.com/LordJawsh"));
		        startActivity(intent);
		        return true;
		    case R.id.menu_twitter_ben:
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://twitter.com/bjwebb67"));
		        startActivity(intent);
		        return true;
		    case R.id.menu_email:
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:josh@fifteenandahalf.com,bjwebb67@googlemail.com"));
		        startActivity(intent);
		        return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
	}
}