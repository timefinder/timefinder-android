package org.timefinder.timefinder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import com.admob.android.ads.AdManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class TimeFinder extends Activity implements OnClickListener {
	TextView searchText;
	public static final String[] LOCATIONS = new String[] { "Manchester", "London" };
	public static final String[] LOCATION_URLS = new String[] { "http://manchester.timefinder.org/", "http://london.timefinder.org/" };
	public static HashMap<String,Void> favStops;
	public static String FAV_FILENAME = "favstops";
	
	public static void updateFavStops(FileOutputStream fos) {
		ObjectOutputStream out;
		try {
			out = new ObjectOutputStream(fos);
			out.writeObject(favStops);
			out.close();
			fos.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}
	
	@Override
	public void onDestroy() {
		try {
			TimeFinder.updateFavStops(openFileOutput(TimeFinder.FAV_FILENAME, Context.MODE_PRIVATE));
		} catch (FileNotFoundException e) {
		}
		super.onDestroy();
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	if (TimeFinder.favStops == null) {
    		FileInputStream fis;
			try {
				fis = openFileInput(TimeFinder.FAV_FILENAME);
	    		ObjectInputStream in = new ObjectInputStream(fis);
	    		TimeFinder.favStops = (HashMap<String,Void>)in.readObject();
	    		in.close();
	    		Log.w("Plings", "starredPlings restored");
			} catch (FileNotFoundException e) {
				TimeFinder.favStops = new HashMap<String,Void>();
	    		Log.w("Plings", "starredPlings created");
			} catch (IOException e) {
			} catch (ClassNotFoundException e) {
			}
    	}
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        AdManager.setTestDevices( new String[] { AdManager.TEST_EMULATOR } );
        
        searchText = (TextView) findViewById(R.id.routesearch_text);
        Button b = (Button) findViewById(R.id.routesearch_button);
        b.setOnClickListener(this);
        
        Button i = (Button) findViewById(R.id.info);
        i.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(), About.class);
		        startActivity(intent);
			}
        });
        
        Spinner locationSpinner = (Spinner) findViewById(R.id.location_spinner);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, LOCATIONS);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(adapter1);
        
		SharedPreferences settings = getSharedPreferences("timefinder_prefs", 0);
        locationSpinner.setSelection(settings.getInt("location_position", 0));
        searchText.setText(settings.getString("route_number", ""));
    }

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(this, RouteSearch.class);
		Spinner s1 = (Spinner) findViewById(R.id.location_spinner);
		SharedPreferences settings = getSharedPreferences("timefinder_prefs", 0);
	    SharedPreferences.Editor editor = settings.edit();
	    editor.putInt("location_position", s1.getSelectedItemPosition());
	    editor.putString("serverurl", LOCATION_URLS[s1.getSelectedItemPosition()]);
		editor.putString("route_number", searchText.getText().toString());
	    editor.commit();
		intent.putExtra("route_number", searchText.getText().toString());
        startActivity(intent);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
		    case R.id.menu_info:
				Intent intent = new Intent(this, About.class);;
		        startActivity(intent);
		        return true;
		    case R.id.quit:
		        finish();
		        return true;
		    default:
		        return super.onOptionsItemSelected(item);
		    }
	}
}