package org.timefinder.timefinder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class JourneyMap extends MapActivity {
	String routeNumber;
	String unescapedName;
	String escapedName;
	List<String> stopCodes;
	List<String> stopNames;
	
	List<Overlay> mapOverlays;
	Drawable drawable;
	MyItemizedOverlay itemizedOverlay;
	
	private class DownloadDataTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... v) {
			itemizedOverlay = new MyItemizedOverlay(JourneyMap.this, drawable);
			SharedPreferences settings = getSharedPreferences("timefinder_prefs", 0);
			String serverurl = settings.getString("serverurl", getString(R.string.serverurl));
			String urlstring = serverurl+"map_api.php?journey="+escapedName+"&service="+routeNumber;
			URL url;
			try {
				url = new URL(urlstring);
			} catch (MalformedURLException e2) {
				return "Internal Error";
			}
			
			InputStream inputStream;
			try {
				inputStream = url.openStream();
				InputStreamReader irs = new InputStreamReader(inputStream);
				BufferedReader br = new BufferedReader(irs);
				String line;
				stopCodes = new ArrayList<String>();
				stopNames = new ArrayList<String>();
				while ((line = br.readLine()) != null) {
					String[] row = line.split(",");
					if (row.length >= 4) {
						stopCodes.add(row[0]);
						stopNames.add(row[1].trim());
						GeoPoint point = new GeoPoint((int)(Double.parseDouble(row[2])*1e6), (int)(Double.parseDouble(row[3])*1e6));
						Log.w("JourneyMap", point.getLatitudeE6()+","+point.getLongitudeE6());
						OverlayItem overlayitem = new OverlayItem(point, row[0], row[0]);
						itemizedOverlay.addOverlay(overlayitem);
					}
				}
			} catch(FileNotFoundException e) {
			    return "API Error";
			} catch (IOException e1) {
				return "Network Error";
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (result == null || result == "") {
				mapOverlays.add(itemizedOverlay);
				MapView map = (MapView) findViewById(R.id.mapview);
				MapController mc = map.getController();
				mc.zoomToSpan(itemizedOverlay.getLatSpanE6(), itemizedOverlay.getLonSpanE6());
				mc.setCenter(itemizedOverlay.getCenter());
				
				ViewSwitcher vs = (ViewSwitcher) findViewById(R.id.switcher);
				if (vs.getDisplayedChild() == 0) {
					vs.showNext();
				}
			}
			else {
				Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
			}
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		
		MapView mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		
		mapOverlays = mapView.getOverlays();
		drawable = this.getResources().getDrawable(R.drawable.mappointer);
		
		Bundle extras = getIntent().getExtras();
		routeNumber = extras.getString("route_number");
		unescapedName = extras.getString("unescaped_name");
		escapedName = extras.getString("escaped_name");
		
		new DownloadDataTask().execute();
	}
	
	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	public void launchStop(int id) {
		String stopName = stopNames.get(id);
		String stopCode = stopCodes.get(id);
		Intent intent = new Intent(this, RouteStop.class);
		intent.putExtra("route_number", routeNumber);
		intent.putExtra("unescaped_name", unescapedName);
		intent.putExtra("escaped_name", escapedName);
		intent.putExtra("stop_name", stopName.trim());
		intent.putExtra("stop_code", stopCode);
        startActivity(intent);
	}
}