package org.timefinder.timefinder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class RouteJourney extends Activity implements OnItemClickListener, OnItemLongClickListener {
	String routeNumber;
	String unescapedName;
	String escapedName;
	List<String> stopCodes;
	List<String> stopNames;
	
	private class DownloadDataTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... v) {
			SharedPreferences settings = getSharedPreferences("timefinder_prefs", 0);
			String serverurl = settings.getString("serverurl", getString(R.string.serverurl));
			String urlstring = serverurl+"routes/"+routeNumber+"/"+escapedName+".csv";
			URL url;
			try {
				url = new URL(urlstring);
			} catch (MalformedURLException e2) {
				return "Internal Error";
			}
			
			InputStream inputStream;
			try {
				inputStream = url.openStream();
				InputStreamReader irs = new InputStreamReader(inputStream);
				BufferedReader br = new BufferedReader(irs);
				String line;
				stopCodes = new ArrayList<String>();
				stopNames = new ArrayList<String>();
				ArrayList<String> stopCodesCache = new ArrayList<String>();
				ArrayList<String >stopNamesCache = new ArrayList<String>();
				HashMap<String, Void> favStops = TimeFinder.favStops;
				while ((line = br.readLine()) != null) {
					String[] row = line.split(",");
					if (row.length >= 2) {
						if (favStops.containsKey(row[0])) {
							stopCodes.add(row[0]);
							stopNames.add("♥ "+row[1].trim());
						}
						else {
							stopCodesCache.add(row[0]);
							stopNamesCache.add(row[1].trim());
						}
					}
				}
				for (String stopCode: stopCodesCache) stopCodes.add(stopCode);
				for (String stopName : stopNamesCache) stopNames.add(stopName);
			} catch(FileNotFoundException e) {
			    return "API Error";
			} catch (IOException e1) {
				return "Network Error";
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (result == null || result == "") {
				ListView lv = (ListView) findViewById(R.id.results);
				lv.setAdapter(new ArrayAdapter<String>(RouteJourney.this, R.layout.list_item, R.id.list_textview, stopNames));
				lv.setOnItemClickListener(RouteJourney.this);
				lv.setOnItemLongClickListener(RouteJourney.this);
				
				ViewSwitcher vs = (ViewSwitcher) findViewById(R.id.switcher);
				if (vs.getDisplayedChild() == 0) {
					vs.showNext();
				}
			}
			else {
				Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
			}
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		Bundle extras = getIntent().getExtras();
		routeNumber = extras.getString("route_number");
		unescapedName = extras.getString("unescaped_name");
		escapedName = extras.getString("escaped_name");
		
		TextView t = (TextView) findViewById(R.id.route_number);
		t.setText(routeNumber);

		new DownloadDataTask().execute();
	}
	
	@Override
	public void onItemClick(AdapterView<?> av, View v, int i, long id) {
		try {
			String stopName = stopNames.get(i);
			String stopCode = stopCodes.get(i);
			stopName = stopName.replace("♥ ", "");
			Intent intent = new Intent(this, RouteStop.class);
			intent.putExtra("route_number", routeNumber);
			intent.putExtra("unescaped_name", unescapedName);
			intent.putExtra("escaped_name", escapedName);
			intent.putExtra("stop_name", stopName.trim());
			intent.putExtra("stop_code", stopCode);
	        startActivity(intent);
		}
		catch (IndexOutOfBoundsException e) {
			
		}
	}
	
	@Override
	public boolean onItemLongClick(AdapterView<?> av, View v, int i,
			long id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.journey, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
		    case R.id.menu_map:
		    	Intent intent = new Intent(this, JourneyMap.class);
				intent.putExtra("route_number", routeNumber);
				intent.putExtra("unescaped_name", unescapedName);
				intent.putExtra("escaped_name", escapedName);
		        startActivity(intent);
		    default:
		        return super.onOptionsItemSelected(item);
		    }
	}
}
