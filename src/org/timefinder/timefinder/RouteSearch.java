package org.timefinder.timefinder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class RouteSearch extends Activity implements OnItemClickListener, OnItemLongClickListener {    
	List<String> unescapedNames;
	List<String> escapedNames;
	String routeNumber;
	
	private class DownloadDataTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... v) {
			SharedPreferences settings = getSharedPreferences("timefinder_prefs", 0);
			String serverurl = settings.getString("serverurl", getString(R.string.serverurl));
			String urlstring = serverurl+"routes/"+routeNumber+"/";
			URL url;
			try {
				url = new URL(urlstring);
			} catch (MalformedURLException e2) {
				return "Internal Error";
			}
			InputStream inputStream;
			unescapedNames = new ArrayList<String>();
			escapedNames = new ArrayList<String>();
			try {
				inputStream = url.openStream();
				InputStreamReader irs = new InputStreamReader(inputStream);
				BufferedReader br = new BufferedReader(irs);
				String line;
				Pattern pattern = Pattern.compile(".*href=\"([^\"]+).csv\".*");
				while ((line = br.readLine()) != null) {
					//out += line+"\n";
					Log.w("RouteSearch", line);
					Matcher matcher = pattern.matcher(line);
					if (matcher.matches()) {
						String m = matcher.group(1);
						escapedNames.add( m );
						unescapedNames.add( URLDecoder.decode(m) );
					}
					else {
					}
				}
			} catch (FileNotFoundException e) {
			    return "Unrecognised Bus Number";
			} catch (IOException e1) {
				return "Network Error";
			}			
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (result == null || result == "") {
				ListView lv = (ListView) findViewById(R.id.results);
				lv.setAdapter(new ArrayAdapter<String>(RouteSearch.this, R.layout.list_item, R.id.list_textview, unescapedNames));
				lv.setOnItemClickListener(RouteSearch.this);
				lv.setOnItemLongClickListener(RouteSearch.this);
				
				ViewSwitcher vs = (ViewSwitcher) findViewById(R.id.switcher);
				if (vs.getDisplayedChild() == 0) {
					vs.showNext();
				}
			}
			else {
				Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
			}
		}
		
	}
	
	public void loadingDone() {
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

		Bundle extras = getIntent().getExtras();
		routeNumber = extras.getString("route_number");
		TextView t = (TextView) findViewById(R.id.route_number);
		t.setText(routeNumber);
		new DownloadDataTask().execute();
	}

	@Override
	public void onItemClick(AdapterView<?> av, View v, int i, long id) {
		try {
			String escapedName = escapedNames.get(i);
			String unescapedName = unescapedNames.get(i);
			Intent intent = new Intent(this, RouteJourney.class);
			intent.putExtra("route_number", routeNumber);
			intent.putExtra("unescaped_name", unescapedName);
			intent.putExtra("escaped_name", escapedName);
	        startActivity(intent);
		}
		catch (IndexOutOfBoundsException e) {
			
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> av, View v, int i,
			long id) {
		// TODO Auto-generated method stub
		return false;
	}
    
}